<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes by soengsouy
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::view('/dashboard', 'dashboard')->middleware('auth')->name('dashboard');
Route::group(['middleware' => 'auth:admin'], function () {
});

Auth::routes();

//============================== Login Socialite ===========================//
// Route::get('/','App\Http\Controllers\Socialite\LoginController@redirectToProvider')->name('login.facebook');
// Route::get('/callback','App\Http\Controllers\Socialite\LoginController@handleProviderCallback');

// -----------------------------user role-----------------------------------------
Route::get('userRole','App\Http\Controllers\HomeController@userRole')->name('userRole');
Route::get('dashboard','App\Http\Controllers\HomeController@index')->name('dashboard');

// ======================== avatar ======================== //
Route::post('profile', 'App\Http\Controllers\HomeController@update_avatar')->name('profile');

// -----------------------------login-----------------------------------------
Route::get('/login', 'App\Http\Controllers\Auth\LoginController@login')->name('login');
Route::post('/login', 'App\Http\Controllers\Auth\LoginController@authenticate');
Route::get('/logout', 'App\Http\Controllers\Auth\LoginController@logout')->name('logout');

// ------------------------------register---------------------------------------
Route::get('/register', 'App\Http\Controllers\Auth\RegisterController@register')->name('register');
Route::post('/register', 'App\Http\Controllers\Auth\RegisterController@storeUser')->name('register');

// -----------------------------forget password ------------------------------
Route::get('forget-password', 'App\Http\Controllers\Auth\ForgotPasswordController@getEmail')->name('forget-password');
Route::post('forget-password', 'App\Http\Controllers\Auth\ForgotPasswordController@postEmail')->name('forget-password');

Route::get('reset-password/{token}', 'App\Http\Controllers\Auth\ResetPasswordController@getPassword');
Route::post('reset-password', 'App\Http\Controllers\Auth\ResetPasswordController@updatePassword');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/user-profile', [App\Http\Controllers\HomeController::class, 'viewProfile'])->name('user-profile');


// ----------------------------- view student ------------------------------
Route::get('form/student/new', [App\Http\Controllers\StudentController::class, 'viewStudent'])->name('form/student/new');
// ----------------------------- view techer ------------------------------
Route::get('form/techer/new', [App\Http\Controllers\TecherController::class, 'viewTecher'])->name('form/techer/new');
// ----------------------------- view guidance ------------------------------
Route::get('form/guidance/new', [App\Http\Controllers\GuidanceController::class, 'viewGuidance'])->name('form/guidance/new');
// ----------------------------- view complaint ------------------------------
Route::get('form/complaint/new', [App\Http\Controllers\ComplaintController::class, 'viewComplaint'])->name('form/complaint/new');
// ----------------------------- view offence ------------------------------
Route::get('form/offence/new', [App\Http\Controllers\OffenceController::class, 'viewOffence'])->name('form/offence/new');

// -----------------------------save information student ------------------------------
Route::post('form/student/save', [App\Http\Controllers\StudentController::class, 'store'])->name('form/student/save');
// -----------------------------save information teacher ------------------------------
Route::post('form/techer/save', [App\Http\Controllers\TecherController::class, 'store'])->name('form/techer/save');
// -----------------------------save information guidance counciling ------------------------------
Route::post('form/guidance/save', [App\Http\Controllers\GuidanceController::class, 'store'])->name('form/guidance/save');
// -----------------------------save information complaint ------------------------------
Route::post('form/complaint/save', [App\Http\Controllers\ComplaintController::class, 'store'])->name('form/complaint/save');
// -----------------------------save information offence ------------------------------
Route::post('form/offence/save', [App\Http\Controllers\OffenceController::class, 'store'])->name('form/offence/save');

// -----------------------------report student ------------------------------
Route::get('report/student/view', [App\Http\Controllers\StudentController::class, 'reportStudent'])->name('report/student/view');
// -----------------------------report teacher ------------------------------
Route::get('report/techer/view', [App\Http\Controllers\TecherController::class, 'reportTeacher'])->name('report/techer/view');
// -----------------------------report guidance counciling ------------------------------
Route::get('report/guidance/view', [App\Http\Controllers\GuidanceController::class, 'reportGuidance'])->name('report/guidance/view');
// -----------------------------report complaint complaint ------------------------------
Route::get('report/complaint/view', [App\Http\Controllers\ComplaintController::class, 'reportComplaint'])->name('report/complaint/view');
// -----------------------------report offence offence ------------------------------
Route::get('report/offence/view', [App\Http\Controllers\OffenceController::class, 'reportOffence'])->name('report/offence/view');

// -----------------------------view detail student ------------------------------
Route::get('report/student/view/detail/{id}', [App\Http\Controllers\StudentController::class, 'viewDetail'])->name('report/student/view/detail');
// -----------------------------view detail teacher ------------------------------
Route::get('report/techer/view/detail/{id}', [App\Http\Controllers\TecherController::class, 'viewDetail'])->name('report/techer/view/detail');
// -----------------------------view detail guidance ------------------------------
Route::get('report/guidance/view/detail/{id}', [App\Http\Controllers\GuidanceController::class, 'viewDetail'])->name('report/guidance/view/detail');
// -----------------------------view detail complaint ------------------------------
Route::get('report/complaint/view/detail/{id}', [App\Http\Controllers\ComplaintController::class, 'viewDetail'])->name('report/complaint/view/detail');
// -----------------------------view detail offence ------------------------------
Route::get('report/offence/view/detail/{id}', [App\Http\Controllers\OffenceController::class, 'viewDetail'])->name('report/offence/view/detail');

// -----------------------------edit student ------------------------------
Route::get('report/student/view/edit/{id}', [App\Http\Controllers\StudentController::class, 'editStudent'])->name('report/student/view/edit');
// -----------------------------edit teacher ------------------------------
Route::get('report/techer/view/edit/{id}', [App\Http\Controllers\TecherController::class, 'editTeacher'])->name('report/techer/view/edit');
// -----------------------------edit guidance counciling ------------------------------
Route::get('report/guidance/view/edit/{id}', [App\Http\Controllers\GuidanceController::class, 'editGuidance'])->name('report/guidance/view/edit');
// -----------------------------edit complaint ------------------------------
Route::get('report/complaint/view/edit/{id}', [App\Http\Controllers\ComplaintController::class, 'editComplaint'])->name('report/complaint/view/edit');
// -----------------------------edit offence ------------------------------
Route::get('report/offence/view/edit/{id}', [App\Http\Controllers\OffenceController::class, 'editOffence'])->name('report/offence/view/edit');

// -----------------------------update student ------------------------------
Route::post('form/student/update', [App\Http\Controllers\StudentController::class, 'updateStudent'])->name('form/student/update');
// -----------------------------update teacher ------------------------------
Route::post('form/techer/update', [App\Http\Controllers\TecherController::class, 'updateTeacher'])->name('form/techer/update');
// -----------------------------update guidance counciling ------------------------------
Route::post('form/guidance/update', [App\Http\Controllers\GuidanceController::class, 'updateGuidance'])->name('form/guidance/update');
// -----------------------------update complaint ------------------------------
Route::post('form/complaint/update', [App\Http\Controllers\ComplaintController::class, 'updateComplaint'])->name('form/complaint/update');
// -----------------------------update offence ------------------------------
Route::post('form/offence/update', [App\Http\Controllers\OffenceController::class, 'updateOffence'])->name('form/offence/update');

//============================== route maintainane =============================//
// ----------------------------- view user role ------------------------------
Route::get('group/user/role/mng', [App\Http\Controllers\UserManagementController::class, 'viewUserRole'])->name('group/user/role/mng');
// ----------------------------- view group user ------------------------------
Route::get('group/user/mng', [App\Http\Controllers\UserManagementController::class, 'viewUserGroup'])->name('group/user/mng');