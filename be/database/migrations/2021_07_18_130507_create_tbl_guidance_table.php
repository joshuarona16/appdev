<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblGuidanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_guidance', function (Blueprint $table) {
            $table->id();
            $table->string('stud_id')->nullable();
            $table->string('full_name')->nullable();
            $table->string('sex')->nullable();
            $table->string('email')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('age')->nullable();
            $table->string('course')->nullable();
            $table->string('section')->nullable();
            $table->string('school_year')->nullable();
            $table->string('dates')->nullable();
            $table->string('date_of_council')->nullable();
            $table->string('schedule_of_council')->nullable();
            $table->string('problem')->nullable();
            $table->string('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_guidance');
    }
}
