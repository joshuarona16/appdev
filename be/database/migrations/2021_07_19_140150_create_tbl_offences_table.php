<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblOffencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_offences', function (Blueprint $table) {
            $table->id();
            $table->string('stud_id')->nullable();
            $table->string('full_name')->nullable();
            $table->string('sex')->nullable();
            $table->string('age')->nullable();
            $table->string('course')->nullable();
            $table->string('section')->nullable();
            $table->string('school_year')->nullable();    
            $table->string('dates')->nullable();
            $table->string('offence')->nullable();
            $table->string('offence_number')->nullable();
            $table->string('date_of_offence')->nullable();
            $table->string('schedule_of_offence')->nullable();
            $table->string('starting_time')->nullable();
            $table->string('end_time')->nullable();
            $table->string('venue')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_offences');
    }
}
