<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblTeacher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_teacher', function (Blueprint $table) {
            $table->id();
            $table->string('teacher_id')->nullable();
            $table->string('full_name')->nullable();
            $table->string('sex')->nullable();
            $table->string('email')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('age')->nullable();
            $table->string('district_brgy')->nullable();
            $table->string('city_municipality')->nullable();
            $table->string('province')->nullable();         
            $table->string('course')->nullable();
            $table->string('department')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_teacher');
    }
}
