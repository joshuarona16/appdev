@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.report_all')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">Edit Teacher Information</h5>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Edit</a></li>
                                <li class="breadcrumb-item"><a href="#!">teacher</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
         
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-block">
                                        <h4 class="sub-title">Teacher Infomation Edit Detail</h4>
                                        
                                        @if (Session::has('message'))
                                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                                        @endif

                                        <form id="validate" name="form" action="{{ route('form/techer/update') }}" method="POST">
                                            @csrf
                                            <input type="text" hidden class="form-control" id="idUpdate" name="idUpdate" value="{{ $value->id }}">
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">ID Number</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="studid" name="studid" value="{{ $value->teacher_id }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Full Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="fullName" name="fullName" value="{{ $value->full_name }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Gender</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="sex" name="sex">
                                                        <option value="male"{{ $value->sex =="male" ? 'selected' : '' }}>Male</option>
                                                        <option value="female" {{ $value->sex =="female" ? 'selected' : '' }}>Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Email</label>
                                                <div class="col-sm-10">
                                                    <input type="email" class="form-control text-danger" id="email" name="email" value="{{ $value->email }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Phone number</label>
                                                <div class="col-sm-10">
                                                    <input type="tel" class="form-control" id="phoneNumber" name="phoneNumber" value="{{ $value->phone_number }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Age</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="age" name="age" value=" {{ $value->age }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">District or Brgy</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="district" name="district" value="{{ $value->district_brgy }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Municipality or City</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="city" name="city" value="{{ $value->city_municipality }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Province</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="province" name="province" value="{{ $value->province }}">
                                                </div>
                                            </div>                                        
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Course Handing</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="course">
                                                        <option value="BSED"    {{ $value->course =="BSED" ? 'selected' : '' }}>BSED</option>
                                                        <option value="BEED"    {{ $value->course =="BEED" ? 'selected' : '' }}>BEED</option>
                                                        <option value="BPED"    {{ $value->course =="BPED" ? 'selected' : '' }}>BPED</option>
                                                        <option value="BTLED"   {{ $value->course =="BTLED" ? 'selected' : '' }}>BTLED</option>
                                                        <option value="BPOS"    {{ $value->course =="BPOS" ? 'selected' : '' }}>BPOS</option>
                                                        <option value="BEED"    {{ $value->course =="BEED" ? 'selected' : '' }}>BEED</option>
                                                        <option value="BACOMM"  {{ $value->course =="BACOMM" ? 'selected' : '' }}>BACOMM</option>
                                                        <option value="BLISS"   {{ $value->course =="BLISS" ? 'selected' : '' }}>BLISS</option>
                                                        <option value="BSIT"    {{ $value->course =="BSIT" ? 'selected' : '' }}>BSIT</option>
                                                        <option value="BAEL"    {{ $value->course =="BAEL" ? 'selected' : '' }}>BAEL</option>
                                                        <option value="BSBIO"   {{ $value->course =="BSBIO" ? 'selected' : '' }}>BSBIO</option>
                                                        <option value="BS ENTREPRENUERSHIP" {{ $value->course =="BS ENTREPRENUERSHIP" ? 'selected' : '' }}>BS ENTREPRENUERSHIP</option>
                                                        <option value="BSHM"    {{ $value->course =="BSHM" ? 'selected' : '' }}>BSHM</option>
                                                        <option value="BSTM"    {{ $value->course =="BSTM" ? 'selected' : '' }}>BSTM</option>
                                                        <option value="BSHRM"   {{ $value->course =="BSHRM" ? 'selected' : '' }}>BSHRM</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Department</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="department">
                                                        <option value="COE"  {{ $value->department =="COE" ? 'selected' : '' }}>COE</option>
                                                        <option value="CAS" {{ $value->department =="CAS" ? 'selected' : '' }}>CAS</option>
                                                        <option value="CME"  {{ $value->department =="CME" ? 'selected' : '' }}>CME</option>
                                                    </select>
                                                </div>
                                            </div>
                                               
                                            <div class="form-group row" align="right">
                                                <label class="col-sm-2 col-form-label"></label>
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-sm btn-success"><i class="icofont icofont-check-circled"></i>Update</button>
                                                    <button type="button" class="btn btn-sm btn-primary"><a href="{{ route('report/techer/view') }}"><i class="icofont icofont-check-circled"></i>Back</a></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>  
window.onload = function() {  

  // ---------------
  // basic usage
  // ---------------
  var $ = new City();
  $.showProvinces("#province");
  $.showCities("#city");

  // ------------------
  // additional methods 
  // -------------------

  // will return all provinces 
  console.log($.getProvinces());
  
  // will return all cities 
  console.log($.getAllCities());
  
  // will return all cities under specific province (e.g Tacloban)
  console.log($.getCities("Tacloban")); 
  
}
</script>
@endsection