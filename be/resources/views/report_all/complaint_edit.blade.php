@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.complaint_report')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">Edit Student Complaint Information</h5>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Edit</a></li>
                                <li class="breadcrumb-item"><a href="#!">Complaint</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
         
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-block">
                                        <h4 class="sub-title">Student Complaint Infomation Edit Detail</h4>
                                        
                                        @if (Session::has('message'))
                                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                                        @endif

                                        <form id="validate" name="form" action="{{ route('form/complaint/update') }}" method="POST">
                                            @csrf
                                            <input type="text" hidden class="form-control" id="idUpdate" name="idUpdate" value="{{ $value->id }}">
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">ID Number</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="studid" name="studid" value="{{ $value->stud_id }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Complainant Name</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="fullName" name="fullName" value="{{ $value->full_name }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Gender</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="sex" name="sex" value="{{ $value->sex }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Age</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="age" name="age" value=" {{ $value->age }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Course</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="course" name="course" value="{{ $value->course }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Year</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="schoolYear" name="schoolYear" value="{{ $value->school_year }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Date of Complaint</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="date_complaint" name="date_complaint" value="{{ $value->date_of_complaint }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Complainee Name</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="c_full_name" name="c_full_name" value="{{ $value->c_full_name }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Gender</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="c_gender" name="c_gender" value="{{ $value->c_gender }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Position</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="c_position" name="c_position" value="{{ $value->c_position }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Reason</label>
                                                <div class="col-sm-10">
                                                    <textarea type="text" rows="5" cols="50" texarea class="form-control" id="reason" name="reason" value="">{{ $value->reason }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row" align="right">
                                                <label class="col-sm-2 col-form-label"></label>
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-sm btn-success"><i class="icofont icofont-check-circled"></i>Update</button>
                                                    <button type="button" class="btn btn-sm btn-primary"><a href="{{ route('report/complaint/view') }}"><i class="icofont icofont-check-circled"></i>Back</a></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection