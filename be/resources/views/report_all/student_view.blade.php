@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.student_report')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">View Student Information</h5>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">View</a></li>
                                <li class="breadcrumb-item"><a href="#!">student</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
         
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-block">
                                        <h4 class="sub-title">Student Infomation View Detail</h4>  
                                        @if (Session::has('message'))
                                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                                        @endif

                                        <form id="validate" name="form" action="{{ route('form/student/save') }}" method="POST">
                                            @csrf
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Student ID</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="studid" name="studid" value="{{ $value->stud_id }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Full Name</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="fullName" name="fullName" value="{{ $value->full_name }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Gender</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="sex" name="sex" value="{{ $value->sex }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Email</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="email" class="form-control text-danger" id="email" name="email" value="{{ $value->email }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Phone number</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="tel" class="form-control" id="phoneNumber" name="phoneNumber" value="{{ $value->phone_number }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Age</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="age" name="age" value=" {{ $value->age }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">City or Municipality</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="city" name="city" value="{{ $value->city_municipality }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Province</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="province" name="province" value="{{ $value->province }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">District</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="district" name="district" value="{{ $value->district_brgy }}">
                                                </div>
                                            </div>
                                        
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Course</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="course" name="course" value="{{ $value->course }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">School year</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="school_year" name="school_year" value="{{ $value->school_year }}">
                                                </div>
                                            </div>
                                               
                                            <div class="form-group row" align="right">
                                                <label class="col-sm-2 col-form-label"></label>
                                                <div class="col-sm-10">
                                                    <button readonly type="button" class="btn btn-sm btn-primary"><a href="{{ route('report/student/view') }}"><i class="icofont icofont-check-circled"></i>Back</a></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection