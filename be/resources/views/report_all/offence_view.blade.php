@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.offence_report')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">View Student Offence Information</h5>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">View</a></li>
                                <li class="breadcrumb-item"><a href="#!">Offence</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
         
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-block">
                                        <h4 class="sub-title">Student Offence Infomation View Detail</h4>  
                                        @if (Session::has('message'))
                                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                                        @endif

                                        <form id="validate" name="form" action="{{ route('form/offence/save') }}" method="POST">
                                            @csrf
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Student ID</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="teacherid" name="teacherid" value="{{ $value->stud_id }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Name</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="fullName" name="fullName" value="{{ $value->full_name }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Gender</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="sex" name="sex" value="{{ $value->sex }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Age</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="age" name="age" value=" {{ $value->age }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Y.C & Section</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="course" name="course" value="{{ $value->school_year }} {{ $value->course }} - {{ $value->section }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Offence</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="offence" name="offence" value="{{ $value->offence }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Offence Number</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="offence_num" name="offence_num" value="{{ $value->offence_number }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Date of Offence</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="date_offence" name="date_offence" value="{{ $value->date_of_offence }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Schedule & Time</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="st" name="st" value="{{ $value->schedule_of_offence }} {{ $value->starting_time }} - {{ $value->end_time }} AM/PM"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Venue</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="venue" name="venue" value="{{ $value->venue }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row" align="right">
                                                <label class="col-sm-2 col-form-label"></label>
                                                <div class="col-sm-10">
                                                    <button readonly type="button" class="btn btn-sm btn-primary"><a href="{{ route('report/offence/view') }}"><i class="icofont icofont-check-circled"></i>Back</a></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection