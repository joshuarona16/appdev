@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.offence_report')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">Edit Student Offence Information</h5>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Edit</a></li>
                                <li class="breadcrumb-item"><a href="#!">Offence</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
         
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-block">
                                        <h4 class="sub-title">Student Offence Infomation Edit Detail</h4>
                                        
                                        @if (Session::has('message'))
                                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                                        @endif

                                        <form id="validate" name="form" action="{{ route('form/offence/update') }}" method="POST">
                                            @csrf
                                            <input type="text" hidden class="form-control" id="idUpdate" name="idUpdate" value="{{ $value->id }}">
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Student ID</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="studid" name="studid" value="{{ $value->stud_id }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="fullName" name="fullName" value="{{ $value->full_name }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Gender</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="sex" name="sex" value="{{ $value->sex }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Age</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="age" name="age" value=" {{ $value->age }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Course</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="course" name="course" value="{{ $value->course }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Section</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="section" name="section" value="{{ $value->section }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Year</label>
                                                <div class="col-sm-10">
                                                    <input readonly type="text" class="form-control" id="schoolYear" name="schoolYear" value="{{ $value->school_year }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Offence</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="offence" name="offence" value="{{ $value->offence }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Offence Number</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="offence_num" name="offence_num">
                                                        <option value="1st Offence" {{ $value->offence_number =="1st Offence" ? 'selected' : '' }}>1st Offence</option>
                                                        <option value="2nd Offence" {{ $value->offence_number =="2nd Offence" ? 'selected' : '' }}>2nd Offence</option>
                                                        <option value="3rd Offence" {{ $value->offence_number =="3rd Offence" ? 'selected' : '' }}>3rd Offence</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Date of Offence</label>
                                                <div class="col-sm-10">
                                                    <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="form-control" id="date_offence" name="date_offence" value="{{ $value->date_of_offence }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Schedule of Offence</label>
                                                <div class="col-sm-10">
                                                    <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="form-control" id="schedule_offence" name="schedule_offence" value="{{ $value->schedule_of_offence }}"/>
                                                </div>
                                            </div>                                       
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Starting Time</label>
                                                <div class="col-sm-10">
                                                <input type="text" onfocus="(this.type='time')" onblur="(this.type='text')" class="form-control" id="starting_time" name="starting_time" value="{{ $value->starting_time }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">End Time</label>
                                                <div class="col-sm-10">
                                                <input type="text" onfocus="(this.type='time')" onblur="(this.type='text')" class="form-control" id="end_time" name="end_time" value="{{ $value->end_time }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Venue</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="venue" name="venue" value="{{ $value->venue }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row" align="right">
                                                <label class="col-sm-2 col-form-label"></label>
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-sm btn-success"><i class="icofont icofont-check-circled"></i>Update</button>
                                                    <button type="button" class="btn btn-sm btn-primary"><a href="{{ route('report/offence/view') }}"><i class="icofont icofont-check-circled"></i>Back</a></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>  
window.onload = function() {  

  // ---------------
  // basic usage
  // ---------------
  var $ = new City();
  $.showProvinces("#province");
  $.showCities("#city");

  // ------------------
  // additional methods 
  // -------------------

  // will return all provinces 
  console.log($.getProvinces());
  
  // will return all cities 
  console.log($.getAllCities());
  
  // will return all cities under specific province (e.g Tacloban)
  console.log($.getCities("Tacloban")); 
  
}
</script>
@endsection