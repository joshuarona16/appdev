@extends('layouts.app')
@section('content')
    <div class="login-form">
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="text-center">
                <a href="" aria-label="Space">
                <img class="mb-3 logo-image" src="{{URL::to('assets/images/lnu-logo.png')}}" alt="Logo" width="150" height="150">
                </a>
            </div>
            <div class="text-center mb-4">
                <h1 class="h3 mb-0">Student Guidance Management Information System</h1>
                <br>
                <p>Fill Out the form to get started.</p>
            </div>
            <div class="js-form-message mb-3">
                <div class="js-focus-state input-group form">
                <div class="input-group-prepend form__prepend">
                    <span class="input-group-text form__text">
                    <i class="fa fa-user form__text-inner"></i>
                    </span>
                </div>
                <input id="name" type="text" class="form-control form__input @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"placeholder="Name" autocomplete="name" autofocus>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="js-form-message mb-3">
                <div class="js-focus-state input-group form">
                <div class="input-group-prepend form__prepend">
                    <span class="input-group-text form__text">
                    <i class="fa fa-user form__text-inner"></i>
                    </span>
                </div>
                    <select class="form-control" id="role_name" name="role_name">
                        <option selected disabled>Role</option>
                        <option disabled value="Administrator">Administrator</option>
                        <option value="Guidance Councelor">Guidance Councelor</option>
                    </select>
                </div>
            </div>
            <div class="js-form-message mb-3">
                <div class="js-focus-state input-group form">
                <div class="input-group-prepend form__prepend">
                    <span class="input-group-text form__text">
                    <i class="fa fa-envelope form__text-inner"></i>
                    </span>
                </div>
                <input type="email" class="form-control form__input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  placeholder="Email" autocomplete="email">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
        <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fa fa-lock"></i>
                        </span>
                    </div>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password"  placeholder="Password" autocomplete="new-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fa fa-key"></i>
                        </span>
                    </div>
                    <input type="password" class="form-control" name="password_confirmation"  placeholder="Confirm Password" autocomplete="new-password">
                </div>
            </div>
            <div class="form-group mb-3">
                <button type="submit" class="btn btn-primary login-btn btn-block">Signup</button>
            </div>
            <div class="text-center mb-3">
                <p class="text-muted">Have an account? <a href="{{route('login')}}">Signin</a></p>
            </div>
            
        </form>
    </div>
@endsection

