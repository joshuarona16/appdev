@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.dashboard')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <!-- order-card start -->
                            <div class="col-md-6 col-xl-3">
                                <div class="card bg-c-blue order-card">
                                    <div class="card-block">
                                        <h6 class="m-b-20">Total Student</h6>
                                        <h2 class="text-right"><i class="fa fa-users f-left"></i><span>{{ $student }}</span></h2>
                                        <p class="m-b-0"><a href="{{ url('report/student/view') }}">SEE MORE</a><span class="f-right"></span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <div class="card bg-c-green order-card">
                                    <div class="card-block">
                                        <h6 class="m-b-20">Student Record</h6>
                                        <h2 class="text-right"><i class="fa fa-files-o f-left"></i><span>1</span></h2>
                                        <p class="m-b-0"><a href="{{ url('report/student/view') }}">SEE MORE</a><span class="f-right"></span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <div class="card bg-c-green order-card">
                                    <div class="card-block">
                                        <h6 class="m-b-20">Complain Record</h6>
                                        <h2 class="text-right"><i class="fa fa-folder-open-o f-left"></i><span>1</span></h2>
                                        <p class="m-b-0"><a href="{{ url('report/complaint/view') }}">SEE MORE</a><span class="f-right"></span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <div class="card bg-c-blue order-card">
                                    <div class="card-block">
                                        <h6 class="m-b-20">Total Teacher</h6>
                                        <h2 class="text-right"><i class="fa fa-users f-left"></i><span>{{ $student }}</span></h2>
                                        <p class="m-b-0"><a href="{{ url('report/techer/view') }}">SEE MORE</a><span class="f-right"></span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <div class="card bg-c-blue order-card">
                                    <div class="card-block">
                                        <h6 class="m-b-20">Activities Record</h6>
                                        <h2 class="text-right"><i class="fa fa-stack-overflow f-left"></i><span>1</span></h2>
                                        <p class="m-b-0"><a href="{{ url('report/student/view') }}">SEE MORE</a><span class="f-right"></span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <div class="card bg-c-yellow order-card">
                                    <div class="card-block">
                                        <h6 class="m-b-20">Total Complaint Record</h6>
                                        <h2 class="text-right"><i class="fa fa-files-o f-left"></i><span>1</span></h2>
                                        <p class="m-b-0"><a href="{{ url('report/complaint/view') }}">SEE MORE</a><span class="f-right"></span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <div class="card bg-c-yellow order-card">
                                    <div class="card-block">
                                        <h6 class="m-b-20">Total Offence Record</h6>
                                        <h2 class="text-right"><i class="fa fa-folder-open-o f-left"></i><span>1</span></h2>
                                        <p class="m-b-0"><a href="{{ url('report/offence/view') }}">SEE MORE</a><span class="f-right"></span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <div class="card bg-c-blue order-card">
                                    <div class="card-block">
                                        <h6 class="m-b-20">Registered Account</h6>
                                        <h2 class="text-right"><i class="fa fa-user-secret f-left"></i><span>{{ $user }}</span></h2>
                                        <p class="m-b-0"><a href="{{ url('report/student/view') }}">SEE MORE</a><span class="f-right"></span></p>
                                    </div>
                                </div>
                            </div>
                            <!-- order-card end -->

                            <!-- statustic and process start -->
                            <div class="col-lg-8 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Statistics</h5>
                                        <div class="card-header-right">
                                            <ul class="list-unstyled card-option">
                                                <li><i class="fa fa-chevron-left"></i></li>
                                                <li><i class="fa fa-window-maximize full-card"></i></li>
                                                <li><i class="fa fa-minus minimize-card"></i></li>
                                                <li><i class="fa fa-refresh reload-card"></i></li>
                                                <li><i class="fa fa-times close-card"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <canvas id="Statistics-chart" height="200"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Report and Feedback</h5>
                                    </div>
                                    <div class="card-block">
                                        <span class="d-block text-c-blue f-24 f-w-600 text-center">365247</span>
                                        <canvas id="feedback-chart" height="100"></canvas>
                                        <div class="row justify-content-center m-t-15">
                                            <div class="col-auto b-r-default m-t-5 m-b-5">
                                                <h4>83%</h4>
                                                <p class="text-success m-b-0"><i class="ti-hand-point-up m-r-5"></i>Positive</p>
                                            </div>
                                            <div class="col-auto m-t-5 m-b-5">
                                                <h4>17%</h4>
                                                <p class="text-danger m-b-0"><i class="ti-hand-point-down m-r-5"></i>Negative</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- statustic and process end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection