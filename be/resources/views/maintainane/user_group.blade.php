@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.user_group')

    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">User Group</h5>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Group</a></li>
                                <li class="breadcrumb-item"><a href="#!">Role</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
        
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-block">
                                        <h4 class="sub-title">User Group Information</h4>
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Sex</th>
                                                    <th>Email</th>
                                                    <th>Phone Number</th>
                                                    <th>Age</th>
                                                    <th>Province</th>
                                                    <th>District</th>
                                                    <th>Subject</th>
                                                    <th>School Year</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                <tr>
                                                    <td>sss</td>
                                                    <td>sss</td>
                                                    <td>sss</td>
                                                    <td>sss</td>
                                                    <td>sss</td>
                                                    <td>sss</td>
                                                    <td>sss</td>
                                                    <td>sss</td>
                                                    <td>sss</td>
                                                    <td>sss</td>
                                                   
                                                    <td>
                                                        <a href="#">Edit</a>
                                                        <a href="#">Delete</a>
                                                    </td>
                                                </tr>
                                               
                                            </tbody>                          
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
