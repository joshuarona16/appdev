@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.techer_form')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">Add Teacher Information/Record</h5>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">form</a></li>
                                <li class="breadcrumb-item"><a href="#!">Teacher</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
         
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-block">
                                        <h2 class="sub-title">Add Infomation and Record</h2>
                                        
                                        @if (Session::has('message'))
                                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                                        @endif

                                        <form id="validate" name="form" action="{{ route('form/techer/save') }}" method="POST">
                                            @csrf
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Teacher ID</label>
                                                <div class="col-sm-10">
                                                    <input type="number" class="form-control" id="teacherid" name="teacherid" placeholder="Please Enter Your ID Number">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Full Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="fullName" name="fullName" placeholder="Lastname, Firstname M.I.">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Gender</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="sex" name="sex">
                                                        <option selected disabled>Please select Gender</option>
                                                        <option value="male">Male</option>
                                                        <option value="female">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Email</label>
                                                <div class="col-sm-10">
                                                    <input type="email" class="form-control text-danger" id="email" name="email" placeholder="Please Enter email">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Phone number</label>
                                                <div class="col-sm-10">
                                                    <input type="tel" class="form-control" id="phoneNumber" name="phoneNumber" placeholder="Please Enter phone number">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Age</label>
                                                <div class="col-sm-10">
                                                    <input type="number" class="form-control" id="age" name="age" placeholder="Please Enter age">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Brgy or Destrict</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="district" name="district" placeholder="Please Enter Brgy/Destrict">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Province</label>
                                                <div class="col-sm-10">
                                                    <select type="text" class="form-control" id="province" name="province" placeholder="Please Select Province" required></select>
                                                </div>
                                            </div>  
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Municipality or City</label>
                                                <div class="col-sm-10">
                                                    <select type="text" class="form-control" id="city" name="city" placeholder="Please Select Municipality or City" required></select>
                                                </div>
                                            </div>                    
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Course Handle</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="course">
                                                    <option selected disabled>Please Select Course Teaching</option>
                                                        <option value="" disabled selected hidden>Please Select Course Teaching</option>
                                                        <option value="BSED">BSED</option>
                                                        <option value="BEED">BEED</option>
                                                        <option value="BPED">BPED</option>
                                                        <option value="BTLED">BTLED</option>
                                                        <option value="BPOS">BPOS</option>
                                                        <option value="BEED">BEED</option>
                                                        <option value="BACOMM">BACOMM</option>
                                                        <option value="BLISS">BLISS</option>
                                                        <option value="BSIT">BSIT</option>
                                                        <option value="BAEL">BAEL</option>
                                                        <option value="BSBIO">BSBIO</option>
                                                        <option value="BS ENTREPRENUERSHIP">BS ENTREPRENUERSHIP</option>
                                                        <option value="BSHM">BSHM</option>
                                                        <option value="BSTM">BSTM</option>
                                                        <option value="BSHRM">BSHRM</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Department</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="department">
                                                        <option selected disabled>Please Select Department</option>
                                                        <option value="" disabled selected hidden>Please Select Department</option>
                                                        <option value="COE">COE</option>
                                                        <option value="CAS">CAS</option>
                                                        <option value="CME">CME</option>
                                                    </select>
                                                </div>
                                            </div>
                                               
                                            <div class="form-group row" align="right">
                                                <label class="col-sm-2 col-form-label"></label>
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-sm btn-primary"><i class="icofont icofont-check-circled"></i>Save</button>
                                                    <button type= "reset"class="btn btn-sm btn-danger"><i class="icofont icofont-warning-alt"></i>Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $("form#validate").validate({
        rules: {
            teacherid: {
                    required: true,
                },
            fullName: {
                    required: true,
                },
            sex:{
                required: true,
            },
            email:{
                required: true,
            },
            phoneNumber:{
                required: true,
            },
            age:{
                required: true,
            },
            city:{
                required: true,
            }, 
            province:{
                required: true,
            }, 
            district:{
                required: true,
            },
            course:{
                required: true,
            }, 
            department:{
                required: true,
            },    
                    
        },
        messages: {
            teacherid: {
                    required: "Please Enter ID Number ",
                },
            fullName: {
                    required: "Please Enter Your Full Name",
                },
            sex:{
                required: "Please Select Gender",
            },
            email:{
                required: "Please Enter Email",
            },
            phoneNumber:{
                required: "Please Enter Phone Number",
            },
            age:{
                required: "Please Enter Age",
            },
            city:{
                required: "Please Select City or Municipality",
            },
            province:{
                required: "Please Select Province",
            },
            district:{
                required: "Please Enter Brgy/Destric",
            },
            course:{
                required: "Please Select Course Handling/Teaching",
            },
            department:{
                required: "Please Select Department",
            },           
        },
        
    });
    </script>
    <script>  
window.onload = function() {  

  // ---------------
  // basic usage
  // ---------------
  var $ = new City();
  $.showProvinces("#province");
  $.showCities("#city");

  // ------------------
  // additional methods 
  // -------------------

  // will return all provinces 
  console.log($.getProvinces());
  
  // will return all cities 
  console.log($.getAllCities());
  
  // will return all cities under specific province (e.g Tacloban)
  console.log($.getCities("Tacloban")); 
  
}
</script>
@endsection
