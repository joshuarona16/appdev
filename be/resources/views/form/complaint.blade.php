@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.complaint_form')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">Add Student Complaint Information/Record</h5>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">form</a></li>
                                <li class="breadcrumb-item"><a href="#!">Complaint</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
         
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-block">
                                        <h2 class="sub-title">Add Infomation and Record</h2>
                                        
                                        @if (Session::has('message'))
                                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                                        @endif

                                        <form role="form" id="validate" name="form" action="{{ route('form/complaint/save') }}" method="POST">
                                            @csrf
                                            <div class="form-group">
                                            <p align="justify" style="font-size:18px;">
                                                    Schools need to make sure that they have outstanding reputations. One way to ensure this is to give 
                                                the students exactly what they need. Students need proper education, decent facilities, and educated 
                                                teachers. It’s up to the school to make sure that the quality of such characteristics is high enough 
                                                to guarantee that the students get what they deserve. Student Concern / Complaint Forms are used 
                                                mostly by those students who wish to raise concerns of any issue within campus that should be looked 
                                                into by the school staff.
                                            </p>
                                            </div>
                                            <hr>
                                            <br>
                                            <p style="font-size:18px;">
                                                Complainant
                                            </p>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Student ID</label>
                                                <div class="col-sm-10">
                                                    <input type="number" class="form-control" id="studid" name="studid" placeholder="Please Enter Your ID Number"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Complainant Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="fullName" name="fullName" placeholder="Lastname, Firstname M.I."/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Gender</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="sex" name="sex">
                                                        <option selected disabled>Please select Gender</option>
                                                        <option value="male">Male</option>
                                                        <option value="female">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Age</label>
                                                <div class="col-sm-10">
                                                    <input type="number" class="form-control" id="age" name="age" placeholder="Please Enter age"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Course</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="course">
                                                    <option selected disabled>Please Select Course</option>
                                                        <option value="" disabled selected hidden>Please Select Course</option>
                                                        <option value="BSED">BSED</option>
                                                        <option value="BEED">BEED</option>
                                                        <option value="BPED">BPED</option>
                                                        <option value="BTLED">BTLED</option>
                                                        <option value="BPOS">BPOS</option>
                                                        <option value="BEED">BEED</option>
                                                        <option value="BACOMM">BACOMM</option>
                                                        <option value="BLISS">BLISS</option>
                                                        <option value="BSIT">BSIT</option>
                                                        <option value="BAEL">BAEL</option>
                                                        <option value="BSBIO">BSBIO</option>
                                                        <option value="BS ENTREPRENUERSHIP">BS ENTREPRENUERSHIP</option>
                                                        <option value="BSHM">BSHM</option>
                                                        <option value="BSTM">BSTM</option>
                                                        <option value="BSHRM">BSHRM</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">School Year</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="schoolYear">
                                                        <option selected disabled>Please Select Year</option>
                                                        <option value="" disabled selected hidden>Please Select Year</option>
                                                        <option value="First Year">First Year</option>
                                                        <option value="Second Year">Second Year</option>
                                                        <option value="Third Year">Third Year</option>
                                                        <option value="Fourth Year">Fourth Year</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <p style="font-size:18px;">
                                                Complainee
                                            </p>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Date</label>
                                                <div class="col-sm-10">
                                                    <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="form-control" id="dates" name="dates" placeholder="Please select Date"/>
                                                </div>
                                            </div> 
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Date of Complaint</label>
                                                <div class="col-sm-10">
                                                    <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="form-control" id="date_complaint" name="date_complaint" placeholder="Please select Date of Complaint"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Complainee</label>
                                                <div class="col-sm-10">
                                                <input type="text" class="form-control" id="c_full_name" name="c_full_name" placeholder="Please Enter Name of Complainee"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Gender</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="c_gender" name="c_gender">
                                                        <option selected disabled>Please Select Gender</option>
                                                        <option value="male">Male</option>
                                                        <option value="female">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Position</label>
                                                <div class="col-sm-10">
                                                <input type="text" class="form-control" id="c_position" name="c_position" placeholder="Please Enter Position of Complainee in the University"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Reason</label>
                                                <div class="col-sm-10">
                                                    <textarea type="text" rows="5" cols="50" texarea class="form-control" id="reason" name="reason" placeholder="Please Enter Reason of Complaint"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group row" align="right">
                                                <label class="col-sm-2 col-form-label"></label>
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-sm btn-primary"><i class="icofont icofont-check-circled"></i>Save</button>
                                                    <button type= "reset"class="btn btn-sm btn-danger"><i class="icofont icofont-warning-alt"></i>Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $("form#validate").validate({
        rules: {
            studid: {
                    required: false,
                },
            fullName: {
                    required: false,
                },
            sex:{
                required: true,
            },
            age:{
                required: true,
            },
            course:{
                required: true,
            }, 
            schoolYear:{
                required: true,
            },    
            dates:{
                required: true,
            }, 
            date_complaint:{
                required: true,
            },
            c_full_name:{
                required: true,
            },
            c_gender:{
                required: true,
            }, 
            c_position:{
                required: true,
            },  
            reason:{
                required: true,
            },   
        },
        messages: {
            studid: {
                    required: "Please Enter ID Number ",
                },
            fullName: {
                    required: "Please Your Full Name",
                },
            sex:{
                required: "Please Select Gender",
            },
            age:{
                required: "Please Enter Age",
            },
            course:{
                required: "Please Select Course",
            },
            schoolYear:{
                required: "Please Select Year",
            },
            dates:{
                required: "Please Select Date",
            }, 
            date_complaint:{
                required: "Please Select Date of Complaint",
            }, 
            c_full_name:{
                required: "Please Enter Complaint Name",
            },   
            c_gender:{
                required: "Please Select Complaint Gender",
            }, 
            c_position:{
                required:"Please Enter Position in the University",
            },
            reason:{
                required:"Please Enter Reason",
            },         
        },
        
    });
    </script>
@endsection
