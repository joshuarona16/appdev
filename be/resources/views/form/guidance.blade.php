@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.guidance_counciling_form')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">Add Student Guidance Counciling Information/Record</h5>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">form</a></li>
                                <li class="breadcrumb-item"><a href="#!">Counciling</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
         
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-block">
                                        <h2 class="sub-title">Add Infomation and Record</h2>
                                        
                                        @if (Session::has('message'))
                                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                                        @endif

                                        <form role="form" id="validate" name="form" action="{{ route('form/guidance/save') }}" method="POST">
                                            @csrf
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Student ID</label>
                                                <div class="col-sm-10">
                                                    <input type="number" class="form-control" id="studid" name="studid" placeholder="Please Enter Your ID Number"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Full Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="fullName" name="fullName" placeholder="Lastname, Firstname M.I."/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Gender</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="sex" name="sex">
                                                        <option selected disabled>Please select Gender</option>
                                                        <option value="male">Male</option>
                                                        <option value="female">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Age</label>
                                                <div class="col-sm-10">
                                                    <input type="number" class="form-control" id="age" name="age" placeholder="Please Enter age"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Email</label>
                                                <div class="col-sm-10">
                                                    <input type="email" class="form-control text-danger" id="email" name="email" placeholder="Please Enter email"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Phone number</label>
                                                <div class="col-sm-10">
                                                    <input type="number" class="form-control" id="phoneNumber" name="phoneNumber" placeholder="Please Enter phone number"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Course</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="course">
                                                    <option selected disabled>Please Select Course</option>
                                                        <option value="" disabled selected hidden>Please Select Course</option>
                                                        <option value="BSED">BSED</option>
                                                        <option value="BEED">BEED</option>
                                                        <option value="BPED">BPED</option>
                                                        <option value="BTLED">BTLED</option>
                                                        <option value="BPOS">BPOS</option>
                                                        <option value="BEED">BEED</option>
                                                        <option value="BACOMM">BACOMM</option>
                                                        <option value="BLISS">BLISS</option>
                                                        <option value="BSIT">BSIT</option>
                                                        <option value="BAEL">BAEL</option>
                                                        <option value="BSBIO">BSBIO</option>
                                                        <option value="BS ENTREPRENUERSHIP">BS ENTREPRENUERSHIP</option>
                                                        <option value="BSHM">BSHM</option>
                                                        <option value="BSTM">BSTM</option>
                                                        <option value="BSHRM">BSHRM</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Section</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="section" name="section" placeholder="Please Enter Section"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">School Year</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="schoolYear">
                                                        <option selected disabled>Please Select Year</option>
                                                        <option value="" disabled selected hidden>Please Select Year</option>
                                                        <option value="First Year">First Year</option>
                                                        <option value="Second Year">Second Year</option>
                                                        <option value="Third Year">Third Year</option>
                                                        <option value="Fourth Year">Fourth Year</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Date</label>
                                                <div class="col-sm-10">
                                                    <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="form-control" id="dates" name="dates" placeholder="Please select Date"/>
                                                </div>
                                            </div> 
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Date of Counciling</label>
                                                <div class="col-sm-10">
                                                    <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="form-control" id="date_counciling" name="date_counciling" placeholder="Please select Date of Counciling"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Time of Schedule</label>
                                                <div class="col-sm-10">
                                                    <input type="text" onfocus="(this.type='time')" onblur="(this.type='text')" class="form-control" id="schedule_counciling" name="schedule_counciling" placeholder="Please select Schedule of Counciling"/>
                                                </div>
                                            </div> 
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Problems</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="problem">
                                                        <option selected disabled>Please Select Personal/Social Problem</option>
                                                        <option value="" disabled selected hidden>Please Select Personal/Social Problem</option>
                                                        <option value="Anger Management">Anger Management</option>
                                                        <option value="Bullying">Bullying</option>
                                                        <option value="Negative Attitude">Negative Attitude</option>
                                                        <option value="Personal Hygiene">Personal Hygiene</option>
                                                        <option value="Adjustment">Adjustment</option>
                                                        <option value="Honesty">Honesty</option>
                                                        <option value="Health">Health</option>
                                                        <option value="Anxiety">Anxiety</option>
                                                        <option value="Theif/Vandalism">Theif/Vandalism</option>
                                                        <option value="Uncooperative/Defiant">Uncooperative/Defiant</option>
                                                        <option value="Others">Others</option>
                                                    </select>
                                                </div>
                                            </div>   
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Comments</label>
                                                <div class="col-sm-10">
                                                    <textarea type="text" rows="5" cols="50" texarea class="form-control" id="comment" name="comment" placeholder="Please Enter Comment"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row" align="right">
                                                <label class="col-sm-2 col-form-label"></label>
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-sm btn-primary"><i class="icofont icofont-check-circled"></i>Save</button>
                                                    <button type= "reset"class="btn btn-sm btn-danger"><i class="icofont icofont-warning-alt"></i>Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $("form#validate").validate({
        rules: {
            studid: {
                    required: true,
                },
            fullName: {
                    required: true,
                },
            sex:{
                required: true,
            },
            email:{
                required: true,
            },
            phoneNumber:{
                required: true,
            },
            age:{
                required: true,
            },
            course:{
                required: true,
            }, 
            section:{
                required: true,
            },
            schoolYear:{
                required: true,
            },    
            dates:{
                required: true,
            }, 
            date_counciling:{
                required: true,
            },
            schedule_counciling:{
                required: true,
            },
            problem:{
                required: true,
            }, 
            comment:{
                required: true,
            },     
        },
        messages: {
            studid: {
                    required: "Please Enter ID Number ",
                },
            fullName: {
                    required: "Please Your Full Name",
                },
            sex:{
                required: "Please Select Gender",
            },
            email:{
                required: "Please Enter Email",
            },
            phoneNumber:{
                required: "Please Enter Phone Number",
            },
            age:{
                required: "Please Enter Age",
            },
            course:{
                required: "Please Select Course",
            },
            section:{
                required: "Please Select Course",
            },
            schoolYear:{
                required: "Please Select Year",
            },
            dates:{
                required: "Please Select Date",
            }, 
            date_counciling:{
                required: "Please Select Date of Counciling",
            }, 
            schedule_counciling:{
                required: "Please Select Schedule of Counciling",
            },   
            problem:{
                required: "Please Select Personal/Social Problem",
            }, 
            comment:{
                required:"Please Enter Comment",
            },         
        },
        
    });
    </script>
@endsection
