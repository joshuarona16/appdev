<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;

class GuidanceController extends Controller
{
    // view guidance_councilling
    public function viewGuidance()
    {
        return view('form.guidance');
    }
    // store data information
    public function store(Request $request)
    {
        $studid                 =   $request->studid;
        $fullName               =   $request->fullName;
        $sex                    =   $request->sex;
        $email                  =   $request->email;
        $phoneNumber            =   $request->phoneNumber;
        $age                    =   $request->age;
        $course                 =   $request->course;
        $section                =   $request->section;  
        $schoolYear             =   $request->schoolYear;
        $dates                  =   $request->dates;
        $date_counciling        =   $request->date_counciling;
        $schedule_counciling    =   $request->schedule_counciling;
        $problem                =   $request->problem;
        $comment                =   $request->comment;

        $GuidanceStore   =   [
            'stud_id'               => $request->studid,
            'full_name'             => $request->fullName,
            'sex'                   => $request->sex,
            'email'                 => $request->email,
            'phone_number'          => $request->phoneNumber,
            'age'                   => $request->age,
            'course'                => $request->course,
            'section'                => $request->section,
            'school_year'           => $request->schoolYear,
            'dates'                 => $request->dates,
            'date_of_council'       => $request->date_counciling,
            'schedule_of_council'   => $request->schedule_counciling,
            'problem'               => $request->problem,
            'comments'               => $request->comment
        ];
        DB::table('tbl_guidance')->insert($GuidanceStore);
        Session::flash('message', "Data has been insert successful!.");
        return Redirect::back();
        // dd($studentStore);
    }
    
    // report student 
    public function reportGuidance()
    {
        $tbl_guidance    =   DB::table('tbl_guidance')->get();
        return view('report_all.guidance_report',compact('tbl_guidance'));
    }
    
    // view detail
    public function viewDetail($id)
    {
        $tbl_guidance    =   DB::table('tbl_guidance')
        ->where('id',$id)
        ->get();
        foreach($tbl_guidance as $value)
        return view('report_all.guidance_view',compact('value'));
    }
    // edit detail
    public function editGuidance($id)
    {
        $tbl_guidance    =   DB::table('tbl_guidance')
        ->where('id',$id)
        ->get();
        foreach($tbl_guidance as $value)
        return view('report_all.guidance_edit',compact('value'));
    }

    // update student
    public function updateGuidance(Request $request)
    {
        $updateGuidance   =   [

            'id'                    => $request->idUpdate,
            'full_name'             => $request->fullName,
            'sex'                   => $request->sex,
            'email'                 => $request->email,
            'phone_number'          => $request->phoneNumber,
            'age'                   => $request->age,
            'course'                => $request->course,
            'section'                => $request->section,
            'school_year'           => $request->schoolYear,
            'dates'                 => $request->date,
            'date_of_council'       => $request->date_counciling,
            'schedule_of_council'   => $request->schedule_counciling,
            'problem'               => $request->problem,
            'comments'               => $request->comment
        ];
        // dd($updateStudent);
        DB::table('tbl_guidance')->where('id',$request->idUpdate)->update($updateGuidance);
        $request->session()->flash('message', 'Data has been update successful!.');
        return redirect::back();
    }
}
