<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;
class ComplaintController extends Controller
{
    // view complaint
    public function viewComplaint()
    {
        return view('form.complaint');
    }
     // store data information
     public function store(Request $request)
     {
         $studid                 =   $request->studid;
         $fullName               =   $request->fullName;
         $sex                    =   $request->sex;
         $age                    =   $request->age;
         $course                 =   $request->course;  
         $schoolYear             =   $request->schoolYear;
         $dates                  =   $request->dates;
         $date_complaint         =   $request->date_complaint;
         $c_full_name            =   $request->c_full_name;
         $c_gender               =   $request->c_gender;
         $c_position             =   $request->c_position;
         $reason                 =   $request->reason;
 
         $ComplaintStore   =   [
             'stud_id'               => $request->studid,
             'full_name'             => $request->fullName,
             'sex'                   => $request->sex,
             'age'                   => $request->age,
             'course'                => $request->course,
             'school_year'           => $request->schoolYear,
             'dates'                 => $request->dates,
             'date_of_complaint'    => $request->date_complaint,
             'c_full_name'           => $request->c_full_name,
             'c_gender'              => $request->c_gender,
             'c_position'            => $request->c_position,
             'reason'                => $request->reason
         ];
         DB::table('tbl_complaint')->insert($ComplaintStore);
         Session::flash('message', "Data has been insert successful!.");
         return Redirect::back();
         // dd($studentStore);
     }
     
     // report student 
     public function reportComplaint()
     {
         $tbl_complaint    =   DB::table('tbl_complaint')->get();
         return view('report_all.complaint_report',compact('tbl_complaint'));
     }
     
     // view detail
     public function viewDetail($id)
     {
         $tbl_complaint    =   DB::table('tbl_complaint')
         ->where('id',$id)
         ->get();
         foreach($tbl_complaint as $value)
         return view('report_all.complaint_view',compact('value'));
     }
     // edit detail
     public function editComplaint($id)
     {
         $tbl_complaint    =   DB::table('tbl_complaint')
         ->where('id',$id)
         ->get();
         foreach($tbl_complaint as $value)
         return view('report_all.complaint_edit',compact('value'));
     }
 
     // update student
     public function updateComplaint(Request $request)
     {
         $updateComplaint   =   [
 
             'id'                    => $request->idUpdate,
             'full_name'             => $request->fullName,
             'sex'                   => $request->sex,
             'age'                   => $request->age,
             'course'                => $request->course,
             'school_year'           => $request->schoolYear,
             'dates'                 => $request->date,
             'date_of_complaint'     => $request->date_complaint,
             'c_full_name'           => $request->c_full_name,
             'c_gender'              => $request->c_gender,
             'c_position'            => $request->c_position,
             'reason'                => $request->reason
         ];
         // dd($updateStudent);
         DB::table('tbl_complaint')->where('id',$request->idUpdate)->update($updateComplaint);
         $request->session()->flash('message', 'Data has been update successful!.');
         return redirect::back();
     }
}
