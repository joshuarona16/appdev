<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;

class StudentController extends Controller
{
    // view student
    public function viewStudent()
    {
        return view('form.student');
    }
    // store data information
    public function store(Request $request)
    {
        $studid         =   $request->studid;
        $fullName       =   $request->fullName;
        $sex            =   $request->sex;
        $email          =   $request->email;
        $phoneNumber    =   $request->phoneNumber;
        $age            =   $request->age;
        $city           =   $request->city;
        $province       =   $request->province;
        $district       =   $request->district;
        $course         =   $request->course;  
        $schoolYear     =   $request->schoolYear;

        $studentStore   =   [
            'stud_id'         => $request->studid,
            'full_name'         => $request->fullName,
            'sex'               => $request->sex,
            'email'             => $request->email,
            'phone_number'      => $request->phoneNumber,
            'age'               => $request->age,
            'city_municipality' => $request->city,
            'province'          => $request->province,
            'district_brgy'     => $request->district,
            'course'            => $request->course,
            'school_year'       => $request->schoolYear
        ];
        DB::table('tbl_student')->insert($studentStore);
        Session::flash('message', "Data has been insert successful!.");
        return Redirect::back();
        // dd($studentStore);
    }
    
    // report student 
    public function reportStudent()
    {
        $tbl_student    =   DB::table('tbl_student')->get();
        return view('report_all.student_report',compact('tbl_student'));
    }
    
    // view detail
    public function viewDetail($id)
    {
        $tbl_student    =   DB::table('tbl_student')
        ->where('id',$id)
        ->get();
        foreach($tbl_student as $value)
        return view('report_all.student_view',compact('value'));
    }
    // edit detail
    public function editStudent($id)
    {
        $tbl_student    =   DB::table('tbl_student')
        ->where('id',$id)
        ->get();
        foreach($tbl_student as $value)
        return view('report_all.student_edit',compact('value'));
    }

    // update student
    public function updateStudent(Request $request)
    {
        $updateStudent   =   [

            'id'                => $request->idUpdate,
            'full_name'         => $request->fullName,
            'sex'               => $request->sex,
            'email'             => $request->email,
            'phone_number'      => $request->phoneNumber,
            'age'               => $request->age,
            'city_municipality' => $request->city,
            'province'          => $request->province,
            'district_brgy'     => $request->district,
            'course'           => $request->course,
            'school_year'       => $request->school_year
        ];
        // dd($updateStudent);
        DB::table('tbl_student')->where('id',$request->idUpdate)->update($updateStudent);
        $request->session()->flash('message', 'Data has been update successful!.');
        return redirect::back();
    }
}
