<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;
class OffenceController extends Controller
{
     // view offence
     public function viewOffence()
     {
         return view('form.offence');
     }
      // store data information
      public function store(Request $request)
      {
          $studid                 =   $request->studid;
          $fullName               =   $request->fullName;
          $sex                    =   $request->sex;
          $age                    =   $request->age;
          $course                 =   $request->course;
          $section                =   $request->section;    
          $schoolYear             =   $request->schoolYear;
          $dates                  =   $request->dates;
          $offence                =   $request->offence;
          $offence_num            =   $request->offence_num;
          $date_offence           =   $request->date_offence;
          $schedule_offence       =   $request->schedule_offence;
          $starting_time          =   $request->starting_time;
          $end_time               =   $request->end_time;
          $venue                  =   $request->venue;
          
  
          $OffenceStore   =   [
              'stud_id'               => $request->studid,
              'full_name'             => $request->fullName,
              'sex'                   => $request->sex,
              'age'                   => $request->age,
              'course'                => $request->course,
              'section'               => $request->section,
              'school_year'           => $request->schoolYear,
              'dates'                 => $request->dates,
              'offence'               => $request->offence,
              'offence_number'        => $request->offence_num,
              'date_of_offence'       => $request->date_offence,
              'schedule_of_offence'   => $request->schedule_offence,
              'starting_time'         => $request->starting_time,
              'end_time'              => $request->end_time,
              'venue'                 => $request->venue
          ];
          DB::table('tbl_offences')->insert($OffenceStore);
          Session::flash('message', "Data has been insert successful!.");
          return Redirect::back();
          // dd($offenceStore);
      }
      
      // report offence 
      public function reportOffence()
      {
          $tbl_offences    =   DB::table('tbl_offences')->get();
          return view('report_all.offence_report',compact('tbl_offences'));
      }
      
      // view detail
      public function viewDetail($id)
      {
          $tbl_offences    =   DB::table('tbl_offences')
          ->where('id',$id)
          ->get();
          foreach($tbl_offences as $value)
          return view('report_all.offence_view',compact('value'));
      }
      // edit detail
      public function editOffence($id)
      {
          $tbl_offences    =   DB::table('tbl_offences')
          ->where('id',$id)
          ->get();
          foreach($tbl_offences as $value)
          return view('report_all.offence_edit',compact('value'));
      }
  
      // update offence
      public function updateOffence(Request $request)
      {
          $updateOffence   =   [
  
              'id'                    => $request->idUpdate,
              'full_name'             => $request->fullName,
              'sex'                   => $request->sex,
              'age'                   => $request->age,
              'course'                => $request->course,
              'section'               => $request->section,
              'school_year'           => $request->schoolYear,
              'dates'                 => $request->dates,
              'offence'               => $request->offence,
              'offence_number'        => $request->offence_num,
              'date_of_offence'       => $request->date_offence,
              'schedule_of_offence'   => $request->schedule_offence,
              'starting_time'         => $request->starting_time,
              'end_time'              => $request->end_time,
              'venue'                 => $request->venue
          ];
          // dd($updateOffence);
          DB::table('tbl_offences')->where('id',$request->idUpdate)->update($updateOffence );
          $request->session()->flash('message', 'Data has been update successful!.');
          return redirect::back();
      }
}
