<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UserManagementController extends Controller
{
    // viewUserRole
    public function viewUserRole()
    {
        $useRole    =   DB::table('users')->get();
        return view('maintainane.user_role',compact('useRole'));
    }

    //viewUserGroup
    public function viewUserGroup()
    {
        return view('maintainane.user_group');
    }
}
