<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;

class TecherController extends Controller
{
    // view form techer
    public function viewTecher()
    {
        return view('form.techer');
    }
    // store data information
    public function store(Request $request)
    {
        $teacherid      =   $request->teacherid;
        $fullName       =   $request->fullName;
        $sex            =   $request->sex;
        $email          =   $request->email;
        $phoneNumber    =   $request->phoneNumber;
        $age            =   $request->age;
        $city           =   $request->city;
        $province       =   $request->province;
        $district       =   $request->district;
        $course         =   $request->course;  
        $department     =   $request->department;

        $techerStore   =   [
            'teacher_id'        => $request->teacherid,
            'full_name'         => $request->fullName,
            'sex'               => $request->sex,
            'email'             => $request->email,
            'phone_number'      => $request->phoneNumber,
            'age'               => $request->age,
            'district_brgy'     => $request->district,
            'city_municipality' => $request->city,
            'province'          => $request->province,  
            'course'            => $request->course,
            'department'        => $request->department
        ];
        DB::table('tbl_teacher')->insert($techerStore);
        Session::flash('message', "Data has been insert successful!.");
        return Redirect::back();
        // dd($studentStore);
    }

    // report teacher 
    public function reportTeacher()
    {
        $tbl_teacher    =   DB::table('tbl_teacher')->get();
        return view('report_all.techer_report',compact('tbl_teacher'));
    }
    
    // view detail
    public function viewDetail($id)
    {
        $tbl_teacher    =   DB::table('tbl_teacher')
        ->where('id',$id)
        ->get();
        foreach($tbl_teacher as $value)
        return view('report_all.techer_view',compact('value'));
    }
    // edit detail
    public function editTeacher($id)
    {
        $tbl_teacher    =   DB::table('tbl_teacher')
        ->where('id',$id)
        ->get();
        foreach($tbl_teacher as $value)
        return view('report_all.techer_edit',compact('value'));
    }

    // update student
    public function updateTeacher(Request $request)
    {
        $updateTeacher   =   [

            'id'                => $request->idUpdate,
            'full_name'         => $request->fullName,
            'sex'               => $request->sex,
            'email'             => $request->email,
            'phone_number'      => $request->phoneNumber,
            'age'               => $request->age,
            'district_brgy'     => $request->district,
            'city_municipality' => $request->city,
            'province'          => $request->province,        
            'course'           => $request->course,
            'department'       => $request->department
        ];
        // dd($updateStudent);
        DB::table('tbl_teacher')->where('id',$request->idUpdate)->update($updateTeacher);
        $request->session()->flash('message', 'Data has been update successful!.');
        return redirect::back();
    }
}
